import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';
import MainComponent from './components/MainComponent';
import Products from './components/Products';

function App() {

  return (
    <MainComponent />
  );
}

export default App;
