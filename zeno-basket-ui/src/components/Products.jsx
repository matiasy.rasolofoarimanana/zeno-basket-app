import { useState, useEffect } from 'react'


export default function Products(props) {
  const [products, setProducts] = useState([])

  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ title: 'React POST Request Example' })
  };

  async function getProducts() {
    const response = await fetch('http://localhost:8080/products', requestOptions)
    const data = await response.json()
    setProducts(data)
  }


  useEffect(() => {
    getProducts()
  }, [])

  return (
    <div>
      <div>
        <h1>List of products</h1>
      </div>
      <ul>
        {products.map((product) => {
          return (
            <div key="product-{product.id}">
              <li>{product.name}</li>
              <button onClick={props.addProductToCart(product.id)}>Add to cart</button>
            </div>
          );
        })}

      </ul>
    </div>
  )
}