package com.zeno.basketapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zeno.basketapi.exception.ZenoException;
import com.zeno.basketapi.response.bean.BasketResponseBean;
import com.zeno.basketapi.service.BasketService;
import com.zeno.basketapi.validator.ProductToBasketBean;

@RestController
@RequestMapping("/basket")
public class BasketController {

	@Autowired
	BasketService basketService;

	@RequestMapping(method = RequestMethod.POST, value = "/addProductToBasket")
	public void addProduct(@RequestBody ProductToBasketBean productToBasketBean) throws ZenoException {
		basketService.addProductToBasket(productToBasketBean);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/getMyBasket")
    public BasketResponseBean getMyBasket() throws ZenoException {
        return basketService.getMyBasket();
    }
	
	@RequestMapping(method = RequestMethod.POST, value = "/cleanMyBasket")
    public void cleanMyBasket() throws ZenoException {
        basketService.cleanMyBasket();
    }
	
	@RequestMapping(method = RequestMethod.POST, value = "/removeProductFromBasket")
    public void removeProductFromBasket(@RequestBody ProductToBasketBean productToBasketBean) throws ZenoException {
        basketService.removeProductFromBasket(productToBasketBean);
    }
	
	@RequestMapping(method = RequestMethod.POST, value = "/decreaseProductCountInBasket")
    public void decreaseProductCountInBasket(@RequestBody ProductToBasketBean productToBasketBean) throws ZenoException {
        basketService.decreaseProductCountInBasket(productToBasketBean);
    }
}
