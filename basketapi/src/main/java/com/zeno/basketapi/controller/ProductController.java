package com.zeno.basketapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zeno.basketapi.bean.ProductBean;
import com.zeno.basketapi.service.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {

	@Autowired
	ProductService productService;

	/**
	 * Service that return list of available products
	 * 
	 * @return {@code List<ProductBean>}
	 */
	@RequestMapping(method = RequestMethod.POST)
	public List<ProductBean> getAll() {
		return productService.findAll();
	}
}
