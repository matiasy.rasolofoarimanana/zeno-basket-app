package com.zeno.basketapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zeno.basketapi.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	
}
