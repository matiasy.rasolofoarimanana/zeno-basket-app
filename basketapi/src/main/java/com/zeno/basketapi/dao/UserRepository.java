package com.zeno.basketapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zeno.basketapi.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
