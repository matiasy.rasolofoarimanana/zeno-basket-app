package com.zeno.basketapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.zeno.basketapi.model.Basket;

public interface BasketRepository extends JpaRepository<Basket, Long> {

    Basket findByUserId(Long userId);

    @Modifying
    @Query("delete from Basket where user.id = ?1")
    void deleteByUserId(Long userId);

}
