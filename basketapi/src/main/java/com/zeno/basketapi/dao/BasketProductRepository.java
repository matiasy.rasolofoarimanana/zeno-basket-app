package com.zeno.basketapi.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.zeno.basketapi.model.BasketProduct;

public interface BasketProductRepository extends JpaRepository<BasketProduct, Long> {

    @Modifying
    @Query(nativeQuery = true, value = "delete from basket_product where basket_id = ?1")
    void deleteAllByBasketId(Long basketId);

    @Modifying
    @Query("delete from BasketProduct where basket.id = ?1 and product.id = ?2")
    void deleteByBasketIdAndProductId(Long basketId, Long productId);

}
