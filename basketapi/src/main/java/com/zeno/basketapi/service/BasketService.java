package com.zeno.basketapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.zeno.basketapi.dao.BasketProductRepository;
import com.zeno.basketapi.dao.BasketRepository;
import com.zeno.basketapi.dao.ProductRepository;
import com.zeno.basketapi.dao.UserRepository;
import com.zeno.basketapi.exception.ZenoException;
import com.zeno.basketapi.model.Basket;
import com.zeno.basketapi.model.BasketProduct;
import com.zeno.basketapi.model.Product;
import com.zeno.basketapi.model.User;
import com.zeno.basketapi.response.bean.BasketProductReponseBean;
import com.zeno.basketapi.response.bean.BasketResponseBean;
import com.zeno.basketapi.validator.ProductToBasketBean;

@Component
public class BasketService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    BasketRepository basketRepository;

    @Autowired
    BasketProductRepository basketProductRepository;

    Logger logger = LoggerFactory.getLogger(BasketService.class);

    @Transactional(rollbackFor = ZenoException.class)
    public void addProductToBasket(ProductToBasketBean productToBasketBean) throws ZenoException {
        Optional<User>    userOpt     = userRepository.findById(1L);
        User              currentUser = userOpt.get();
        Optional<Product> productOpt  = productRepository.findById(productToBasketBean.productId);
        if (productOpt.isEmpty()) {
            throw new ZenoException("Product with id %s not found", productToBasketBean.productId);
        }
        Product product = productOpt.get();
        logger.debug("Found Product : {}", product);
        if (product.getStock() <= 0) {
            throw new ZenoException("No stock available for Product %s", product.getDisplayName());
        }
        Basket basket = basketRepository.findByUserId(currentUser.getId());
        logger.debug("Found Basket : {}", basket);
        Basket basketUpdate = (null == basket) ? new Basket() : basket;
        if (null != basket) {
            basketUpdate = basket;
        } else {
            basketUpdate = new Basket();
            basketUpdate.setUser(currentUser);
        }
        List<BasketProduct> basketProductUpdates = new ArrayList<BasketProduct>();
        for (BasketProduct basketProduct : basketUpdate.getBasketProducts()) {
            if (basketProduct.getProduct().getId() == product.getId()) {
                basketProduct.setQuantity(basketProduct.getQuantity() + 1);
            }
            basketProductUpdates.add(basketProduct);
        }
        basketRepository.saveAndFlush(basketUpdate);
        if (basketProductUpdates.isEmpty()) {
            BasketProduct basketProduct = new BasketProduct();
            basketProduct.setProduct(product);
            basketProduct.setBasket(basketUpdate);
            basketProduct.setQuantity(1);
            basketProductUpdates.add(basketProduct);
        }
        basketUpdate.setBasketProducts(basketProductUpdates);
        logger.debug("Basket after update : {}", basketUpdate);
        product.setStock(product.getStock() - 1);
        logger.debug("Product after update : {}", product);
    }

    public BasketResponseBean getMyBasket() {
        Optional<User>                 userOpt             = userRepository.findById(1L);
        User                           currentUser         = userOpt.get();
        BasketResponseBean             basketResponseBean  = new BasketResponseBean();
        List<BasketProductReponseBean> productReponseBeans = new ArrayList<>();
        basketResponseBean.setProducts(productReponseBeans);
        Basket basket = basketRepository.findByUserId(currentUser.getId());
        if (null != basket) {
            basket.getBasketProducts().forEach(basketProduct -> {
                productReponseBeans.add(BasketProductReponseBean.getInstance(basketProduct));
            });
        }
        return basketResponseBean;
    }

    @Transactional(rollbackFor = ZenoException.class)
    public void cleanMyBasket() throws ZenoException {
        Optional<User> userOpt     = userRepository.findById(1L);
        User           currentUser = userOpt.get();
        Basket         basket      = basketRepository.findByUserId(currentUser.getId());
        logger.debug("Found Basket : {}", basket);
        if (null == basket) {
            throw new ZenoException("No Basket available for current user", currentUser.getName());
        }
        for (BasketProduct basketProduct : basket.getBasketProducts()) {
            Product product = basketProduct.getProduct();
            product.setStock(product.getStock() + basketProduct.getQuantity());
        }
        basketProductRepository.deleteAllByBasketId(basket.getId());
    }

    @Transactional(rollbackFor = ZenoException.class)
    public void removeProductFromBasket(ProductToBasketBean productToBasketBean) throws ZenoException {
        Optional<User>    userOpt     = userRepository.findById(1L);
        User              currentUser = userOpt.get();
        Optional<Product> productOpt  = productRepository.findById(productToBasketBean.productId);
        if (productOpt.isEmpty()) {
            throw new ZenoException("Product with id %s not found", productToBasketBean.productId);
        }
        Product product = productOpt.get();
        logger.debug("Found Product : {}", product);
        Basket basket = basketRepository.findByUserId(currentUser.getId());
        logger.debug("Found Basket : {}", basket);
        if (null == basket) {
            throw new ZenoException("No Basket available for current user", currentUser.getName());
        }
        for (BasketProduct basketProduct : basket.getBasketProducts()) {
            if (basketProduct.getProduct().equals(product)) {
                product.setStock(product.getStock() + basketProduct.getQuantity());
            }
        }
        basketProductRepository.deleteByBasketIdAndProductId(currentUser.getId(), product.getId());
    }

    @Transactional(rollbackFor = ZenoException.class)
    public void decreaseProductCountInBasket(ProductToBasketBean productToBasketBean) throws ZenoException {
        Optional<User>    userOpt     = userRepository.findById(1L);
        User              currentUser = userOpt.get();
        Optional<Product> productOpt  = productRepository.findById(productToBasketBean.productId);
        if (productOpt.isEmpty()) {
            throw new ZenoException("Product with id %s not found", productToBasketBean.productId);
        }
        Product product = productOpt.get();
        logger.debug("Found Product : {}", product);
        Basket basket = basketRepository.findByUserId(currentUser.getId());
        logger.debug("Found Basket : {}", basket);
        if (null == basket) {
            throw new ZenoException("No Basket available for current user", currentUser.getName());
        }
        BasketProduct basketProductUpdate = null;
        for (BasketProduct basketProduct : basket.getBasketProducts()) {
            if (basketProduct.getProduct().getId() == product.getId()) {
                basketProductUpdate = basketProduct;
            }
        }
        if (null != basketProductUpdate) {
            basketProductUpdate.setQuantity(basketProductUpdate.getQuantity() - 1);
        }
    }

}
