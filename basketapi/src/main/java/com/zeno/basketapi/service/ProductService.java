package com.zeno.basketapi.service;

import static com.zeno.basketapi.bean.ProductBean.getInstance;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zeno.basketapi.bean.ProductBean;
import com.zeno.basketapi.dao.ProductRepository;
import com.zeno.basketapi.model.Product;

@Component
public class ProductService {

	@Autowired
	ProductRepository productRepository;

	public List<ProductBean> findAll() {
		List<Product> productEntities = productRepository.findAll();
		List<ProductBean> products = new ArrayList<ProductBean>();
		productEntities.forEach(productEntity -> products.add(getInstance(productEntity)));
		return products;
	}
	
}
