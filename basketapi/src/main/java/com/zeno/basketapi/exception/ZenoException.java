package com.zeno.basketapi.exception;


public class ZenoException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ZenoException(String message,Object...args) {
        super(String.format(message, args));
    }
}
