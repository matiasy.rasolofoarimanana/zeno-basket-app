package com.zeno.basketapi.response.bean;

import com.zeno.basketapi.model.Product;

public class ProductMinBean {

    private Long id;

    private String name;

    private String displayName;

    public ProductMinBean(Long id, String name, String displayName) {
        super();
        this.id          = id;
        this.name        = name;
        this.displayName = displayName;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public static ProductMinBean getInstance(Product product) {
        return new ProductMinBean(product.getId(), product.getName(), product.getDisplayName());
    }

}
