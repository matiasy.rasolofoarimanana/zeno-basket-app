package com.zeno.basketapi.response.bean;

import com.zeno.basketapi.model.BasketProduct;
import com.zeno.basketapi.model.Product;

public class BasketProductReponseBean {

    private Long productId;

    private String name;

    private String displayName;

    private int quantity;

    public Long getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public static BasketProductReponseBean getInstance(BasketProduct basketProduct) {
        BasketProductReponseBean basketProductReponseBean = new BasketProductReponseBean();
        Product                  product                  = basketProduct.getProduct();
        basketProductReponseBean.setProductId(product.getId());
        basketProductReponseBean.setName(product.getName());
        basketProductReponseBean.setDisplayName(product.getDisplayName());
        basketProductReponseBean.setQuantity(basketProduct.getQuantity());
        return basketProductReponseBean;
    }

}
