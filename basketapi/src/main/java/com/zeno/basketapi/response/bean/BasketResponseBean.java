package com.zeno.basketapi.response.bean;

import java.util.List;

public class BasketResponseBean {

    private List<BasketProductReponseBean> products;

    public List<BasketProductReponseBean> getProducts() {
        return products;
    }

    public void setProducts(List<BasketProductReponseBean> products) {
        this.products = products;
    }

}
