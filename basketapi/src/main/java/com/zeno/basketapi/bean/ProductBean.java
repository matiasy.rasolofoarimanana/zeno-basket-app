package com.zeno.basketapi.bean;

public class ProductBean {

	private Long id;

	private String name;

	private String displayName;

	private double price;

	private String brand;

	private String icon;

	private Integer stock;

	public static ProductBean getInstance(com.zeno.basketapi.model.Product productEntity) {
		ProductBean product = new ProductBean();
		product.setId(productEntity.getId());
		product.setName(productEntity.getName());
		product.setDisplayName(productEntity.getDisplayName());
		product.setPrice(productEntity.getPrice());
		product.setIcon(productEntity.getIcon());
		product.setStock(productEntity.getStock());
		return product;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public double getPrice() {
		return price;
	}

	public String getBrand() {
		return brand;
	}

	public String getIcon() {
		return icon;
	}

	public Integer getStock() {
		return stock;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

}
