# zeno-basket-app

Le code source est divisé en 2 apps .
- basketapi
- zeno-basket-ui

## basketapi

Elle contient l'application Spring boot , qui délivre les end points qui permettent de gérer le panier d'un utilisateur . 

Actuellement la fonctionnalité d'authentification n'a pas été implémenté . 

L'api utilise une base de données Postgres. 

Il faut créer une base de données qui se nomme "basket" au préalable , et selon les configurations spécifiques , modifiez le fichier application.properties.

Un fichier data.sql a été placé dans src/main/resources afin de charger des données par défaut à l'initialisation de la base de données . 

Ci-dessous les end point implementées :
- http://localhost:8080/products
- http://localhost:8080/basket/addProductToBasket
- http://localhost:8080/basket/getMyBasket
- http://localhost:8080/basket/cleanMyBasket
- http://localhost:8080/basket/removeProductFromBasket
- http://localhost:8080/basket/decreaseProductCountInBasket

Une collection postman a été placé sous le chemin ../basketapi/postman-collections pour démontrer l'usage des API Spring boot.

## zeno-basket-ui

L'application React a été initialisé . 

Cependant , que la liste des produits a été implémenté.